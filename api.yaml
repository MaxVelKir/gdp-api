openapi: 3.0.0
info:
  version: "1.0.0"
  title: "gdp"
  license:
    name: MIT
servers:
  - url: "104.248.81.164:80"
paths:
  /api/plural:
    get:
      operationId: list items
      description: lists objects of given type between the limits (uses generated id)
      parameters:
        - name: type
          in: query
          required: true
          schema:
            type: string
            enum:
              - variant
              - patient
              - disease
        - name: limit_start
          in: query
          required: false
          schema:
            type: integer
        - name: limit_end
          in: query
          required: false
          schema:
            type: integer
      responses:
        "200":
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  payload:
                    type: array
                    items:
                      type: object
                  status:
                    type: string
                    enum:
                      - success
        "400":
          description: Invalid query type
          content:
            application/json:
              schema:
                type: object
                properties:
                  payload:
                    type: string
                  status:
                    type: string
                    enum:
                      - failed
    delete:
      operationId: destroy items
      description: Destroys all object of the given type
      parameters:
        - name: type
          in: query
          required: true
          schema:
            type: string
            enum:
              - variant
              - patient
              - disease
              - variant_record
              - filter
              - all
      responses:
        "200":
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  payload:
                    type: string
                  status:
                    type: string
                    enum:
                      - success
        "400":
          description: Invalid query type
          content:
            application/json:
              schema:
                type: object
                properties:
                  payload:
                    type: string
                  status:
                    type: string
                    enum:
                      - failed
  /api/count:
    get:
      operationId: count
      description: Returns object with the number of objects in each model
      responses:
        "200":
          content:
            application/json:
              schema:
                type: object
                properties:
                  Patient:
                    type: integer
                  ClinicalHistory:
                    type: integer
                  Variant:
                    type: integer
                  VariantRecord:
                    type: integer
                  Disease:
                    type: integer
                  Filter:
                    type: integer
                  Gene:
                    type: integer
                  AlleleFrequency:
                    type: integer
          description: OK
  /api/prepopulate_diseases:
    get:
      operationId: prepopulate diseases
      description: Populates the Disease table with objects
      responses:
        "200":
          content:
            application/json:
              schema:
                type: object
                properties:
                  payload:
                    type: string
                  status:
                    type: string
                    enum:
                      - success
          description: OK
        "404":
          content:
            application/json:
              schema:
                type: object
                properties:
                  payload:
                    type: string
                  status:
                    type: string
                    enum:
                      - failed
          description: Source file not found
  /api/extract_vcf:
    post:
      operationId: extract from vcf
      description: main functionallity - extracts data from vcf file and online sources
      parameters:
        - name: patient_id
          in: query
          required: true
          schema:
            type: string
        - name: gender
          in: query
          schema:
            type: string
            enum:
              - M
              - F
        - name: info
          in: query
          schema:
            type: string
      requestBody:
        description: vcf file in body in binary mode
        content:
          binary:
            example: doesnt matter
        required: true
      responses:
        "200":
          content:
            application/json:
              schema:
                type: object
                properties:
                  payload:
                    type: string
                  status:
                    type: string
                    enum:
                      - success
          description: OK
        "404":
          content:
            application/json:
              schema:
                type: object
                properties:
                  payload:
                    type: string
                  status:
                    type: string
                    enum:
                      - failed
          description: Source file not sent
        "500":
          content:
            application/json:
              schema:
                type: object
                properties:
                  payload:
                    type: string
                  status:
                    type: string
                    enum:
                      - failed
          description: Source file not found

components:
  schemas:
    Patient:
      title: Patient
      type: object
      properties:
        patient_id:
          type: string
        gender:
          type: string
          enum:
            - M
            - F
        info:
          type: string
        clinicalhistory:
          $ref: "#/components/schemas/ClinicalHistory"
        variantrecords:
          type: array
          items:
            $ref: "#/components/schemas/VariantRecord"
      required:
        - patient_id
    ClinicalHistory:
      title: Clinical History
      type: object
      properties:
        patient:
          $ref: "#/components/schemas/Patient"
        diseases:
          type: array
          items:
            $ref: "#/components/schemas/Disease"
      required:
        - patient
    Disease:
      title: Disease
      type: object
      properties:
        disease_id:
          type: string
        description:
          type: string
        clinicalhistories:
          type: array
          items:
            $ref: "#/components/schemas/ClinicalHistory"
        clinicalsignificances:
          type: array
          items:
            $ref: "#/components/schemas/ClinicalSignificance"
      required:
        - disease_id
    ClinicalSignificance:
      title: Clinical Significance
      type: object
      properties:
        variant:
          $ref: "#/components/schemas/Variant"
        disease:
          $ref: "#/components/schemas/Disease"
        significance:
          type: string
          enum:
            - B
            - LB
            - P
            - LP
            - V
        evaluated:
          type: string
        review_status:
          type: string
        updated:
          type: string
      required:
        - variant
        - disease
        - significance
    Variant:
      title: Variant
      type: object
      properties:
        variant_id:
          type: string
        dbsnp_id:
          type: integer
        chromosome:
          type: string
        position:
          type: integer
        ref_sequence:
          type: string
        alt_sequence:
          type: string
        is_pseudo:
          type: boolean
        exon:
          type: string
        intron:
          type: string
        consequence:
          type: string
        conseq_details:
          type: string
        variantrecord:
          $ref: "#/components/schemas/VariantRecord"
        genes:
          type: array
          items:
            $ref: "#/components/schemas/Gene"
        allelefrequencies:
          type: array
          items:
            $ref: "#/components/schemas/AlleleFrequency"
        clinicalsignificances:
          type: array
          items:
            $ref: "#/components/schemas/ClinicalSignificance"
      required:
        - variant_id
        - variantrecord
    Gene:
      title: Gene
      type: object
      properties:
        gene_id:
          type: string
        name:
          type: string
        variants:
          type: array
          items:
            $ref: "#/components/schemas/Variant"
      required:
        - gene_id
        - name
    Filter:
      title: Filter
      type: object
      properties:
        name:
          type: string
        phred_quality:
          type: number
          format: float
        variantrecord:
          $ref: "#/components/schemas/VariantRecord"
      required:
        - name
        - phred_quality
        - variantrecord
    VariantRecord:
      title: Variant Record
      type: object
      properties:
        patient:
          $ref: "#/components/schemas/Patient"
        variant:
          $ref: "#/components/schemas/Variant"
        homozygotous:
          type: boolean
        ad:
          type: integer
        dp:
          type: integer
        filters:
          type: array
          items:
            $ref: "#/components/schemas/Filter"
      required:
        - patient
        - variant
    AlleleFrequency:
      title: Allele Frequency
      type: object
      properties:
        variant:
          $ref: "#/components/schemas/Variant"
        pop_gender:
          type: string
          enum:
            - M
            - F
        pop_area:
          type: string
          enum:
            - BGR
            - OVR
            - NFE
            - ""
        frequency:
          type: number
          format: float
        update:
          type: string
